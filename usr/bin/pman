#!/usr/bin/env bash
#######################################################################################################################################################################################
#                                                       ~   P M A N  -  P A C K A G E  D O W N L O A D  H E L P E R   ~                                                                 #
#######################################################################################################################################################################################

TITLE=" ~ Pman Download Helper ~ "

multiDownlist=$(mktemp -uq)

trap 'sudo rm -f ${multiDownlist} /tmp/total.tmp >/dev/null 2>&1' EXIT TERM QUIT  >/dev/null 2>&1

cat <<-EOF > ${multiDownlist}
##  PLEASE ENTER YOUR DESIRED SOURCES AND PACKAGES BELOW :  ( No source assumes pkg is in package database or AUR )

packages=(



 )


## If you wish to clone personal GitHub or GitLab repositories,
## please enter your own GitHub and/or GitLab username below:

# GitHub / GitLab Usernames :

hubname=""
labname=""



## Optionally, you can change the color scheme to your prefrence below.
# Your options are as follows:  ( blue, green, red, yellow, white, pink or random )

color1="blue"      # Total Count
color2="pink"      # Package Name
color3="yellow"    # Package Description


## PREFIXES AND FORMATTING::
# ---

# Pacman/AUR Package   : <package-name>

# Gitlab Repo          : L,<gitlab-username>/<repo-name>
# GitHub Repo          : H,<github-username>/<repo-name>

# Npm Package          : N,<package-name>
# Snap Package         : S,<package-name>
# Flatpak Package      : F,<package-name>
# Python Package       : P,<package-name>

# Personal GitLab Repo : ML,<repo-name>
# Personal GitHub Repo : MH,<repo-name>

# Web File / Torrent   : W,<URL>



## PACKAGE TEMPLATES::

# -------------------------------- #

# Package Support / Wine Packages:

# 'wine'
# 'twine'
# 'wine-mono'
# 'wine-nine'
# 'wine-gecko'
# 'wine-staging'
# 'wine-gallium'
# 'wine-directx'
# 'libappimage'
# 'appimagelauncher '
# 'lib32-vkd3d-tkg-git'
# 'proton-ge-custom-bin'

# -------------------------------- #

# Mutt ( Email Client ) Packages:

# 'libosinfo'
# 'dmg2img'
# 'click'
# 'ardour'
# 'isync'
# 'msmtp'
# 'mailsync'
# 'pam-gnupg'
# 'notmuch'
# 'abook'
# 'urlview'
# 'cronie'
# 'mpop'
# abook

# -------------------------------- #

# QEMU and KVM Packages:

# qemu
# uml-utilities
# virt-manager
# wget
# libguestfs-tools
# p7zip-full
# dmg2img
# libvirt
# qemu-arch-extra
# virt-viewer
# virt-install

# -------------------------------- #




# See /usr/share/doc/pkgdl/README.md for more information on list formatting.

################################################################################################################################
################################################################################################################################
EOF


name="$(echo "${USER}")"


[ -n $(echo $color1) ] || [ ! $color1 ] && color1=blue

[ -n $(echo $color2) ] || [ ! $color2 ] && color2=pink

[ -n $(echo $color3) ] || [ ! $color3 ] && color3=yellow


yellow(){ echo -n  "$(tput bold; tput setaf 3)${*}$(tput sgr0)" ; }

blue()  { echo -n  "$(tput bold; tput setaf 6)${*}$(tput sgr0)" ; }

white() { echo -n  "$(tput bold; tput setaf 7)${*}$(tput sgr0)" ; }

pink()  { echo -n  "$(tput bold; tput setaf 5)${*}$(tput sgr0)" ; }

red()   { echo -en "$(tput bold; tput setaf 1)${*}$(tput sgr0)" ; }

green() { echo -n  "$(tput bold; tput setaf 2)${*}$(tput sgr0)" ; }

msg()   { echo -e  "$(tput bold; tput setaf 2)${*}$(tput sgr0)" ; }

random(){ echo -n  "$(tput bold; tput setaf "$(seq 25|shuf|head -1)") ${*}$(tput sgr0)" ; }

err()   { clear ; echo -e "\n$(tput bold; tput setaf 1)[-] ERROR: ${*}$(tput sgr0)" && exit 1337 ; }


scrstart()    { clear ; echo -e "\n\n" ; green "   Downloading Packages : " ; echo  -e "\n\n" ||
  red "\n [-] ERROR: Could Not Find '"$1"'\n\n" ; }


npminstall()  { sudo npm install -g "$1" >/dev/null 2>&1 && return 0 ||
  ! sudo pacman -Qq npm >/dev/null 2>&1 && err "NPM is not installed. Please install it and try again."  ||
  red "\n [-] ERROR: Could Not Find '"$1"' in NPM database.\n\n" ; }

snpinstall()  { sudo snap install "$1" >/dev/null 2>&1 && return 0 ||
  ! sudo pacman -Qq snapd >/dev/null 2>&1 && err "Snapd is not installed. Please install it and try again." ||
  red "\n [-] ERROR: Could Not Find '"$1"'in Snap database.\n\n" ; }

pkginstall()  { ${helper} -S --needed --noconfirm "$1" >/dev/null 2>&1 && return 0 ||
  $( ${helper} -Si "$1" >/dev/null 2>&1)  &&
  red "\n [-] Errors occurred during install of ${1}. Most likely due to conflicting packages.\n\n" ||
  red "\n [-] ERROR: '"$1"' could not be found in package database.\n\n" ; }

pipinstall()  { sudo python3 -m pip install "$1" >/dev/null 2>&1 && return 0 ||
  ! sudo pacman -Qq python-pip >/dev/null 2>&1 && err "Python-pip is not installed. Please install it and try again." ||
  red "\n [-] ERROR: Could Not Find '"$1"' in Python database.\n\n" ; }

flatinstall()  { sudo flatpak install -y "$1" >/dev/null 2>&1 && return 0 ||
  ! sudo pacman -Qq flatpak >/dev/null 2>&1 && err "Flatpak is not installed. Please install it and try again." ||
  red "\n [-] ERROR: Could Not Find '"$1"' in Flatpak database.\n\n" ; }

labclone()    { git clone https://gitlab.com/"$1".git >/dev/null 2>&1 && return 0 ||
  ! sudo pacman -Qq git >/dev/null 2>&1 && err "Git is not installed. Please install it and try again." ||
   red "\n [-] ERROR: Could Not Find '"$1"' Git repo.\n\n" ; }

mylabclone()  { git clone https://gitlab.com/"$labname"/"$(basename $1)".git >/dev/null 2>&1 && return 0 ||
  ! sudo pacman -Qq git >/dev/null 2>&1 && err "Git is not installed. Please install it and try again." ||
   red "\n [-] ERROR: Could Not Find '"$1"' on Gitlab.\n\n" ; }

myhubclone()  { git clone https://github.com/"$hubname"/"$(basename $1)".git >/dev/null 2>&1 && return 0 ||
  ! sudo pacman -Qq git >/dev/null 2>&1 && err "Git is not installed. Please install it and try again." ||
   red "\n [-] ERROR: Could Not Find '"$1"' on Github.\n\n" ; }

hubclone()    { git clone https://github.com/"$1".git >/dev/null 2>&1 && return 0 ||
  ! sudo pacman -Qq git >/dev/null 2>&1 && err "Git is not installed. Please install it and try again." ||
   red "\n [-] ERROR: Could Not Find '"$1"' Git repo.\n\n" ; }

lnOrTorrent() { aria2c "${1}" >/dev/null 2>&1 && return 0 ||
  ! sudo pacman -Qq aria2 >/dev/null 2>&1 && err "NPM is not installed. Please install it and try again." ||
  red "\n [-] ERROR: "$1", Could not be download.\n\n" ; }


testLocal(){ \
  sudo pacman -Qi ${1} |grep "^Install Date.*" >/dev/null 2>&1 && return 0 || dialog \
    --title "$TITLE" \
    --yes-label 'Yes, Install It.' \
    --no-label 'No Nevermind.' \
    --yesno "\\n${1} cannot be found locally on the system, would you like to install it now?" 6 90 || exit 1

  dialog \
    --title "$TITLE" \
    --infobox "\\nInstalling ${1} ..." 6 35

  sudo pacman -S --noconfirm ${1} >/dev/null 2>&1 && return 0

  ${helper} -S --noconfirm ${1} >/dev/null 2>&1 && return 0 ||
    err "${1} could not be installed. Please install it manually." ; }


printHelp(){ cat /usr/share/doc/ez/ezdl/README && exit 0 ; }

getEditor(){

editorSel=(
  '1' 'Neovim'
  '2' 'Vim'
  '3' 'Nano'
  '4' 'Emacs'
  '5' 'VS Code'
  '6' 'Atom'
  '7' 'Leafpad'
  '8' 'Notepadqq'
  '9' 'Gedit'
  '10' 'Enter Manually'
)

editorChoice=$(dialog \
  --stdout \
  --title "$TITLE " \
  --menu "\\n                Please select your preferred text editor:\\n\\n\\nNOTE: This will only be asked once, after which, can be changed by editing the config file located at '${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc'\\n\\nFor convenience, a terminal based editor is recommended.\\n " 16 80 0 \
  "${editorSel[@]}")

[ -z $editorChoice ] && err "No editor selected ..."

case $editorChoice in
  1) export editor="nvim" ; export editorName="neovim" ;;
  2) export editor="vim" ; export editorName="vim" ;;
  3) export editor="nano" ; export editorName="nano" ;;
  4) export editor="emacs" ; export editorName="emacs" ;;
  5) export editor="code" ; export editorName="code" ;;
  6) export editor="atom" ; export editorName="atom" ;;
  7) export editor="leafpad" ; export editorName="leafpad" ;;
  8) export editor="notepadqq" ; export editorName="notepadqq" ;;
  9) export editor="gedit" ; export editorName="gedit" ;;
  10) export editor=$(dialog --stdout --title "$TITLE " \
    --inputbox "Please enter the full name of your preferred text editor:" 10 65)
      export editorName=${editor} ;;
esac

testLocal ${editorName}

[ ! -f ${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc ] &&
  echo -e "#            ~ PKD Config File ~             #\n\n"\
  >> ${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc

! cat ${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc  >/dev/null 2>&1 | grep -q "editor" &&
  echo "editor=\"${editor}\"" >> ${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc ||
  sed -i "s/editor=.*/editor=\"${editor}\"/" ${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc ; }


getHelper(){

helperSel=(
  '1' 'Yay'
  '2' 'Paru'
  '3' 'Trizen'
  '4' 'Pacaur'
)

helperChoice=$(dialog \
  --stdout \
  --title "$TITLE " \
  --menu "\\n                Please select your preferred AUR helper:\\n\\n\\nNOTE: This will only be asked once, after which, can be changed by editing the config file located at '${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc'\\n " 14 80 0 \
  "${helperSel[@]}")

[ -z $helperChoice ] && err "No editor selected ..."

case $helperChoice in
  1) helperName=yay ; for help in yay yay-bin yay-git; do sudo pacman -Qq $help >/dev/null 2>&1 && export helper=yay && break ; done ;;
  2) helperName=paru ; for help in paru paru-bin paru-git; do sudo pacman -Qq $help >/dev/null 2>&1 && export helper=paru && break ; done ;;
  3) helperName=trizen ; for help in trizen trizen-bin trizen-git; do sudo pacman -Qq $help >/dev/null 2>&1 && export helper=trizen && break ; done ;;
  4) helperName=pacaur ; for help in pacaur pacaur-bin pacaur-git; do sudo pacman -Qq $help >/dev/null 2>&1 && export helper=pacaur && break ; done ;;
esac

[ -z $helper ] && dialog \
  --title "$TITLE" \
  --yes-label 'Yes, Install It.' \
  --no-label 'No Nevermind.' \
  --yesno "\\nThe selected AUR helper was not found on your system, would you like to install it now?" 6 95

fetchHelper=$?

[ $fetchHelper = 0 ] && dialog \
  --title "$TITLE" \
  --infobox "\\nInstalling ${helperName} ..." 6 35 && eval $(\
  sudo pacman -S --noconfirm --needed git >/dev/null 2>&1
[ "$(id -u)" -eq 0 ] && err "Please run this script as a normal user, not with elevated privileges (sudo)."
git clone https://aur.archlinux.org/${helperName}.git /tmp/${helperName}.$$ >/dev/null 2>&1

cd /tmp/${helperName}.$$ &&
  makepkg -sri --noconfirm >/dev/null 2>&1 && cd ~/ &&
  sudo rm -rf /tmp/${helperName}.$$ >/dev/null 2>&1 ||
  err "Could not install ${helperName}. Please install it manually.")

! cat ${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc >/dev/null 2>&1 | grep -q "helper" &&
  echo -e "helper=\"${helperName}\"" >> ${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc ||
  sed -i "s/helper=.*/helper=\"${helperName}\"/" ${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc ; }



# Script Start:


! [ -d ${XDG_DATA_HOME:-$HOME/.local/share}/PKD ] && mkdir -p ${XDG_DATA_HOME:-$HOME/.local/share}/PKD


source ${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc

[ -z ${editor} ] && getEditor

[ -z ${helper} ] && getHelper

source ${XDG_DATA_HOME:-$HOME/.local/share}/PKD/pkdrc



case ${1} in

  -h|--help) printHelp ;;

  -f) echo "packages=(" >${multiDownlist} ;

    for CAT in ${@} ; do

      cat "${CAT}" >>$multiDownlist

    done ; echo ')' >>$multiDownlist

    source ${multiDownlist} ;;

  '') $editor $multiDownlist && source $multiDownlist ;;

  *) packages=( ${@} ) ;;

esac


[ -z $packages ] && err "No packages specified ..."


for x in  "${packages[@]}" ; do

  echo $x >> /tmp/total.tmp

  sed -i '/^\s*$/d;/^#/d' /tmp/total.tmp

 done ; scrstart


while IFS=, read -r src pkg desc viewName ; do

  [[ -z ${pkg} ]] && pkg="${src}" && unset src

  [[ -z ${desc} ]] && [[ -z ${src} ]] && desc="$( ${helper} -Si ${pkg} 2>/dev/null|grep 'Description'|cut -d: -f2-|sed 's/^\s*//')"

  viewName="${pkg}"


  echo ${pkg}|grep -q "^http.*" || echo ${pkg}|grep -q "^www.*" || echo ${pkg}|grep -q "^magnet:\?.*" &&
    src='W' && viewName="$(basename ${pkg})"


  [[ ${src} == 'W' ]] && [[ -z ${desc} ]] && desc="Downloading Link..."

  [[ ${src} == 'MH' ]] && [[ -z ${desc} ]] && desc="Cloning Repo from ${hubname}'s Github ..."

  [[ ${src} == 'ML' ]] && [[ -z ${desc} ]] && desc="Cloning Repo from ${hubname}'s Gitlab..."

  [[ ${src} == 'W' ]] && echo ${pkg} | grep -q "\.torrent$" || echo ${pkg} | grep -q "^magnet:\?.*" &&
    viewName="Downloading Torrent File..." && desc=""


  [[ ${src} == 'L' ]] && [[ -z ${desc} ]] && desc="Cloning from Gitlab..."

  [[ ${src} == 'H' ]] && [[ -z ${desc} ]] && desc="Cloning from Github..."


  ((n=$n+1)) ; total=$(wc -l < /tmp/total.tmp) ;

  $color1 "( $n of $total ) "

  $color2 $viewName &&  [ -n "$(echo "$desc")" ] && green " - " && $color3 "$desc" ; echo ""


case "$src" in

  "W") lnOrTorrent "$pkg" ;; "L") labclone "$pkg"    ;; "ML") mylabclone "$pkg" ;;

  "S") snpinstall "$pkg"  ;; "N") npminstall "$pkg"  ;; "H") hubclone "$pkg"    ;;

  "MH") myhubclone "$pkg" ;;  "") pkginstall "$pkg" ;;  "P") pipinstall "$pkg"  ;;

  "F") flatinstall "$pkg" ;; *) echo -e "\n" &&  err ""$src" is  not a valid option for a source. - skipping: "$src" ..."

    echo -e "\n" ; sleep 2 ;; esac ; done < /tmp/total.tmp || exit 1

msg "\n\n\n[+] Download Complete.\n" && sleep 1 && exit 0 || exit 1


##############################################################################################################################
##############################################################################################################################

